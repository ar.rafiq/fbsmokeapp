var express = require('express');
var router = express.Router();
var multer = require('multer');
var path = require('path');
//var up=multer({ dest: './uploads/'});
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '_' + randomString(8)) //Appending .jpg
  }
})

var upload = multer({ storage: storage });
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('uploadimage');
});
router.get('/healthcheck', function(req, res, next) {
  res.send('OK');
});
router.post("/makeVideo", upload.single('image'),function(req,res){
  res.render('makevideo', { filepath: (req.file.filename) });
  });





/**
 * FLUENT FFMPEG TESTING CODE BELOW
 */


const exec = require('child_process').exec;

var randomString = function (length) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

router.post('/convert', upload.single('video'),function(req,res){
  console.log(req.file,"FILE");
  var _filename = "vid_"+req.file.filename +'.mp4';
  exec(`ffmpeg -i uploads/${req.file.filename} public/downloads/${_filename}`, (err, stdout, stderr) => {
    if (err) {
      res.send({ success: false,message:"Unable to process video"});
      return;
    }
    res.json({ success: true, url: 'http://9xstuff.spotboye.com/downloads/' + _filename});
  });

});


module.exports = router;
