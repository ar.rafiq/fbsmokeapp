  var canvas = document.getElementById('myCanvas');
    var ctx = canvas.getContext('2d');
    var imageObj = new Image();
    var smokeAnim = new Image();
    smokeAnim.src = "../images/smoke-sprites.png";
    //  smokeAnim.onload = function () {
    //    ctx.drawImage(smokeAnim, 180, 100);
    //  };
    imageObj.onload = function () {
      ctx.drawImage(imageObj, 0, 0);

    };
imageObj.src = "<%=filepath%>";

    //     function startSmoking() {
    //       
    //     }


    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;

    var spriteWidth = 9677;
    var spriteHeight = 403;

    var rows = 1;
    var cols = 24;

    var trackRight = 0;
    var trackLeft = 0;

    var width = spriteWidth / cols;
    var height = spriteHeight / rows;
    var curFrame = 0;
    var frameCount = 24;

    var x = 0;
    var y = 0;

    var srcX;
    var srcY;

    var left = false;
    var right = true;

    var speed = 40;

    function updateFrame() {
      curFrame = ++curFrame % frameCount;
      srcX = curFrame * width;
      ctx.clearRect(x, y, width, height);
  
       if(left && x>0){
       srcY = trackLeft * height; 
      // x-=speed; 
       }
      if (right && x < canvasWidth - width) {
        srcY = trackRight * height;
       // x+=speed; 
      }
    }

    function draw() {
      updateFrame();
      ctx.drawImage(imageObj, 0, 0);
      ctx.drawImage(smokeAnim, srcX, 0, 340, 358, 0, 0, 400, 400);
    }
    setInterval(draw, speed);